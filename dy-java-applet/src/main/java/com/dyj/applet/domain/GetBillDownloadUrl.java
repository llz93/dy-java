package com.dyj.applet.domain;

/**
 * 对账单下载地址
 */
public class GetBillDownloadUrl {


    /**
     * <p>账单下载链接</p>
     */
    private String bill_download_url;

    public String getBill_download_url() {
        return bill_download_url;
    }

    public GetBillDownloadUrl setBill_download_url(String bill_download_url) {
        this.bill_download_url = bill_download_url;
        return this;
    }
}
