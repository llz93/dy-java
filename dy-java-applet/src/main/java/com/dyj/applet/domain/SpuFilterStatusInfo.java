package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-29 16:50
 **/
public class SpuFilterStatusInfo {

    /**
     * 过滤原因
     */
    private String reason;
    /**
     * 过滤码
     */
    private Long code;
    /**
     * 是否被过滤
     */
    private Boolean filter;
    /**
     * supplier下的poiId
     */
    private String poi_id;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Boolean getFilter() {
        return filter;
    }

    public void setFilter(Boolean filter) {
        this.filter = filter;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }
}
