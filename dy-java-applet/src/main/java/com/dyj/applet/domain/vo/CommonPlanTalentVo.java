package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.CommonPlanTalent;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 17:33
 **/
public class CommonPlanTalentVo {

    private List<CommonPlanTalent> data;

    /**
     * 数据产出日期
     */
    private String date;

    /**
     * 总页数
     */
    private Integer page_count;
    /**
     * 总达人数
     */
    private Integer total_count;

    public List<CommonPlanTalent> getData() {
        return data;
    }

    public void setData(List<CommonPlanTalent> data) {
        this.data = data;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getPage_count() {
        return page_count;
    }

    public void setPage_count(Integer page_count) {
        this.page_count = page_count;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }
}
