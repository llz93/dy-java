package com.dyj.applet.domain.query;

import com.dyj.applet.domain.OrderExtShopInfo;
import com.dyj.applet.domain.OrderMiniApp;
import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-05-06 14:22
 **/
public class SyncOrderQuery extends BaseQuery {

    /**
     * 小程序形式对接抖音时，该字段必传
     */
    private OrderMiniApp mini_app;
    /**
     * 外部商家信息
     */
    private OrderExtShopInfo ext_shop_info;


    public static SyncOrderQueryBuilder builder() {
        return new SyncOrderQueryBuilder();
    }

    public static class SyncOrderQueryBuilder {
        private OrderMiniApp miniApp;
        private OrderExtShopInfo extShopInfo;
        private Integer tenantId;
        private String clientKey;
        public SyncOrderQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }
        public SyncOrderQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }
        public SyncOrderQueryBuilder miniApp(OrderMiniApp miniApp) {
            this.miniApp = miniApp;
            return this;
        }
        public SyncOrderQueryBuilder extShopInfo(OrderExtShopInfo extShopInfo) {
            this.extShopInfo = extShopInfo;
            return this;
        }
        public SyncOrderQuery build() {
            SyncOrderQuery syncOrderQuery = new SyncOrderQuery();
            syncOrderQuery.setTenantId(tenantId);
            syncOrderQuery.setClientKey(clientKey);
            syncOrderQuery.setMini_app(miniApp);
            syncOrderQuery.setExt_shop_info(extShopInfo);
            return syncOrderQuery;
        }
    }

    public OrderMiniApp getMini_app() {
        return mini_app;
    }

    public void setMini_app(OrderMiniApp mini_app) {
        this.mini_app = mini_app;
    }

    public OrderExtShopInfo getExt_shop_info() {
        return ext_shop_info;
    }

    public void setExt_shop_info(OrderExtShopInfo ext_shop_info) {
        this.ext_shop_info = ext_shop_info;
    }
}
