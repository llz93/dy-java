package com.dyj.applet.domain;

import java.util.List;

/**
 * @author danmo
 * @date 2024-05-06 16:40
 **/
public class SpuExtInfo {

    /**
     * 三方商品Id
     */
    private String spu_ext_id;

    /**
     * 可用门店ID列表，可选，不穿默认商品所在的所有门店都是可用的
     */
    private List<String> supplier_ext_ids;

    public static SpuExtInfoBuilder builder() {
        return new SpuExtInfoBuilder();
    }

    public static class SpuExtInfoBuilder {
        private String spuExtId;
        private List<String> supplierExtIds;

        public SpuExtInfoBuilder spuExtId(String spuExtId) {
            this.spuExtId = spuExtId;
            return this;
        }
        public SpuExtInfoBuilder supplierExtIds(List<String> supplierExtIds) {
            this.supplierExtIds = supplierExtIds;
            return this;
        }

        public SpuExtInfo build() {
            SpuExtInfo spuExtInfo = new SpuExtInfo();
            spuExtInfo.setSpu_ext_id(spuExtId);
            spuExtInfo.setSupplier_ext_ids(supplierExtIds);
            return spuExtInfo;
        }
    }

    public String getSpu_ext_id() {
        return spu_ext_id;
    }

    public void setSpu_ext_id(String spu_ext_id) {
        this.spu_ext_id = spu_ext_id;
    }

    public List<String> getSupplier_ext_ids() {
        return supplier_ext_ids;
    }

    public void setSupplier_ext_ids(List<String> supplier_ext_ids) {
        this.supplier_ext_ids = supplier_ext_ids;
    }
}
