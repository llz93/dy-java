package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 查询营销活动请求值
 */
public class QueryPromotionActivityV2Query extends BaseQuery {

    /**
     * 需要查询的营销活动ID
     */
    private String activity_id;

    public String getActivity_id() {
        return activity_id;
    }

    public QueryPromotionActivityV2Query setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public static QueryPromotionActivityV2QueryBuilder builder(){
        return new QueryPromotionActivityV2QueryBuilder();
    }

    public static final class QueryPromotionActivityV2QueryBuilder {
        private String activity_id;
        private Integer tenantId;
        private String clientKey;

        private QueryPromotionActivityV2QueryBuilder() {
        }

        public QueryPromotionActivityV2QueryBuilder activityId(String activityId) {
            this.activity_id = activityId;
            return this;
        }

        public QueryPromotionActivityV2QueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public QueryPromotionActivityV2QueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public QueryPromotionActivityV2Query build() {
            QueryPromotionActivityV2Query queryPromotionActivityV2Query = new QueryPromotionActivityV2Query();
            queryPromotionActivityV2Query.setActivity_id(activity_id);
            queryPromotionActivityV2Query.setTenantId(tenantId);
            queryPromotionActivityV2Query.setClientKey(clientKey);
            return queryPromotionActivityV2Query;
        }
    }
}
