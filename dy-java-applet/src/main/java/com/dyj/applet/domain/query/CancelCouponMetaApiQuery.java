package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * 删除券模板请求值
 */
public class CancelCouponMetaApiQuery extends BaseQuery {

    /**
     * 小程序appid
     */
    private String app_id;
    /**
     * 抖音开平券模板id，券模板创建接口会返回该字段，和 merchant_meta_no 字段二者选填一个 选填
     */
    private String coupon_meta_id;
    /**
     * 小程序券模板号（开发者内部系统的券模版自增id或uuid），和 coupon_meta_id 字段二者选填一个；都填时，优先根据coupon_meta_id 查询 选填
     */
    private String merchant_meta_no;

    public String getApp_id() {
        return app_id;
    }

    public CancelCouponMetaApiQuery setApp_id(String app_id) {
        this.app_id = app_id;
        return this;
    }

    public String getCoupon_meta_id() {
        return coupon_meta_id;
    }

    public CancelCouponMetaApiQuery setCoupon_meta_id(String coupon_meta_id) {
        this.coupon_meta_id = coupon_meta_id;
        return this;
    }

    public String getMerchant_meta_no() {
        return merchant_meta_no;
    }

    public CancelCouponMetaApiQuery setMerchant_meta_no(String merchant_meta_no) {
        this.merchant_meta_no = merchant_meta_no;
        return this;
    }

    public static CancelCouponMetaApiQueryBuilder builder(){
        return new CancelCouponMetaApiQueryBuilder();
    }

    public static final class CancelCouponMetaApiQueryBuilder {
        private String app_id;
        private String coupon_meta_id;
        private String merchant_meta_no;
        private Integer tenantId;
        private String clientKey;

        private CancelCouponMetaApiQueryBuilder() {
        }

        public CancelCouponMetaApiQueryBuilder appId(String appId) {
            this.app_id = appId;
            return this;
        }

        public CancelCouponMetaApiQueryBuilder couponMetaId(String couponMetaId) {
            this.coupon_meta_id = couponMetaId;
            return this;
        }

        public CancelCouponMetaApiQueryBuilder merchantMetaNo(String merchantMetaNo) {
            this.merchant_meta_no = merchantMetaNo;
            return this;
        }

        public CancelCouponMetaApiQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public CancelCouponMetaApiQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public CancelCouponMetaApiQuery build() {
            CancelCouponMetaApiQuery cancelCouponMetaApiQuery = new CancelCouponMetaApiQuery();
            cancelCouponMetaApiQuery.setApp_id(app_id);
            cancelCouponMetaApiQuery.setCoupon_meta_id(coupon_meta_id);
            cancelCouponMetaApiQuery.setMerchant_meta_no(merchant_meta_no);
            cancelCouponMetaApiQuery.setTenantId(tenantId);
            cancelCouponMetaApiQuery.setClientKey(clientKey);
            return cancelCouponMetaApiQuery;
        }
    }
}
