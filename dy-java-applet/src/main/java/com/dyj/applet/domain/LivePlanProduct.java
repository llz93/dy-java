package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-05-13 16:28
 **/
public class LivePlanProduct {

    /**
     * 商品的定向抽佣率，万分数。可选值范围：商品的商家建立了开环代运营商服合作关系时可设置为0-8000，其他情况可设置为0-2900。
     */
    private Long commission_rate;
    /**
     * 上传小程序商品时返回的抖音内部商品ID。
     */
    private Long product_id;

    public static LivePlanProductBuilder builder() {
        return new LivePlanProductBuilder();
    }

    public static class LivePlanProductBuilder {
        private Long commissionRate;

        private Long productId;

        public LivePlanProductBuilder commissionRate(Long commissionRate) {
            this.commissionRate = commissionRate;
            return this;
        }

        public LivePlanProductBuilder productId(Long productId) {
            this.productId = productId;
            return this;
        }

        public LivePlanProduct build() {
            LivePlanProduct livePlanProduct = new LivePlanProduct();
            livePlanProduct.setCommission_rate(commissionRate);
            livePlanProduct.setProduct_id(productId);
            return livePlanProduct;
        }
    }

    public Long getCommission_rate() {
        return commission_rate;
    }

    public void setCommission_rate(Long commission_rate) {
        this.commission_rate = commission_rate;
    }

    public Long getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Long product_id) {
        this.product_id = product_id;
    }
}
