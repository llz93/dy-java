package com.dyj.applet.domain.query;

import com.dyj.common.domain.query.BaseQuery;

/**
 * @author danmo
 * @date 2024-04-29 16:27
 **/
public class SpuStockQuery extends BaseQuery {

    /**
     * 接入方商品ID
     */
    private String spu_ext_id;

    /**
     * 库存
     */
    private Long stock;

    public static SpuStockQueryBuilder builder() {
        return new SpuStockQueryBuilder();
    }

    public static final class SpuStockQueryBuilder {
        private String spuExtId;
        private Long stock;
        private Integer tenantId;
        private String clientKey;

        public SpuStockQueryBuilder spuExtId(String spuExtId) {
            this.spuExtId = spuExtId;
            return this;
        }

        public SpuStockQueryBuilder stock(Long stock) {
            this.stock = stock;
            return this;
        }

        public SpuStockQueryBuilder tenantId(Integer tenantId) {
            this.tenantId = tenantId;
            return this;
        }

        public SpuStockQueryBuilder clientKey(String clientKey) {
            this.clientKey = clientKey;
            return this;
        }

        public SpuStockQuery build() {
            SpuStockQuery spuStockQuery = new SpuStockQuery();
            spuStockQuery.setSpu_ext_id(spuExtId);
            spuStockQuery.setStock(stock);
            spuStockQuery.setTenantId(tenantId);
            spuStockQuery.setClientKey(clientKey);
            return spuStockQuery;
        }
    }

    public String getSpu_ext_id() {
        return spu_ext_id;
    }

    public void setSpu_ext_id(String spu_ext_id) {
        this.spu_ext_id = spu_ext_id;
    }

    public Long getStock() {
        return stock;
    }

    public void setStock(Long stock) {
        this.stock = stock;
    }
}
