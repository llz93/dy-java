package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.OrientedPlanTalent;

import java.util.List;

public class OrientedPlanTalentVo {

    /**
     * 页数，固定为1
     */
    private Integer page_count;

    /**
     * 计划总数
     */
    private Integer total_count;

    private List<OrientedPlanTalent> data;

    public Integer getPage_count() {
        return page_count;
    }

    public void setPage_count(Integer page_count) {
        this.page_count = page_count;
    }

    public Integer getTotal_count() {
        return total_count;
    }

    public void setTotal_count(Integer total_count) {
        this.total_count = total_count;
    }

    public List<OrientedPlanTalent> getData() {
        return data;
    }

    public void setData(List<OrientedPlanTalent> data) {
        this.data = data;
    }
}
