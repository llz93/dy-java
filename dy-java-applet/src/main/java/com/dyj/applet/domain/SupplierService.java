package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 15:19
 **/
public class SupplierService {

    /**
     * 上线状态(1:上线，2:下线)
     */
    private Integer enable;

    /**
     * 服务类型(201-预约点餐服务, 202-预约订位服务, 203-排队取号服务, 9001-门票预订服务, 9101-团购套餐服务, 9102-领优惠劵服务, 9201-在线预约服务, 9301-外卖服务)
     */
    private Integer service_type;

    /**
     * 服务入口拼接参数
     */
    private SupplierServiceEntry entry;

    public static SupplierServiceBuilder builder() {
        return new SupplierServiceBuilder();
    }

    public static class SupplierServiceBuilder {
        private Integer enable;
        private Integer serviceType;
        private SupplierServiceEntry entry;
        public SupplierServiceBuilder enable(Integer enable) {
            this.enable = enable;
            return this;
        }
        public SupplierServiceBuilder serviceType(Integer serviceType) {
            this.serviceType = serviceType;
            return this;
        }
        public SupplierServiceBuilder entry(SupplierServiceEntry entry) {
            this.entry = entry;
            return this;
        }
        public SupplierService build() {
            SupplierService supplierService = new SupplierService();
            supplierService.setEnable(enable);
            supplierService.setService_type(serviceType);
            supplierService.setEntry(entry);
            return supplierService;
        }
    }

    public Integer getEnable() {
        return enable;
    }

    public void setEnable(Integer enable) {
        this.enable = enable;
    }

    public Integer getService_type() {
        return service_type;
    }

    public void setService_type(Integer service_type) {
        this.service_type = service_type;
    }

    public SupplierServiceEntry getEntry() {
        return entry;
    }

    public void setEntry(SupplierServiceEntry entry) {
        this.entry = entry;
    }
}
